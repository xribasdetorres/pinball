﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Points : MonoBehaviour {

    
    public GameObject BolaPin;
    public GameObject Compuerta;
    public GameObject RampaCompuerta;
    public GameObject Targets;
    public AudioClip Gameover;
    public AudioClip Soundtrack;

    private AudioSource source;
    private Rigidbody rgBolaPin;
    private bool close;
    private bool start;
    private int points;
    private int vidas;
    private int multiplicador;
    private Vector3 PositionBola;
    private Quaternion Quat;
    private Quaternion QuatTar;
    private Vector3 posbolapin;
    private Vector3 Target1;
    private Vector3 Target2;
    private Vector3 Target3;

    // Use this for initialization
    void Start () {

        rgBolaPin = BolaPin.GetComponent<Rigidbody>();
        source = GetComponent<AudioSource>();
        points = 0;
        vidas = 0;
        multiplicador = 0;
        PositionBola.x = 8.98f;
        PositionBola.y = 1.2f;
        PositionBola.z = 0.88f;
        Target1.x = 3.4f;
        Target1.y = 0.721f;
        Target1.z = 14.92f;
        Target2.x = 2.03f;
        Target2.y = 0.721f;
        Target2.z = 14.92f;
        Target3.x = 0.45f;
        Target3.y = 0.721f;
        Target3.z = 14.92f;
        QuatTar.SetAxisAngle(Target1, 0);
        close = false;

    }

    // Update is called once per frame
    void Update()
    {

        
        if (GameObject.FindGameObjectWithTag("BolaPin") == null && vidas >= 1)
        {
           
            Instantiate(BolaPin, PositionBola, Quat);
            close = false;
        }

        if (GameObject.FindGameObjectWithTag("BolaPin") != null)
        {
            posbolapin = GameObject.FindGameObjectWithTag("BolaPin").transform.position;
        }
        if(posbolapin.z >= 5.89f )
        {
            close = true;
        }
        if (start == true)
        {
            multiplicador = 0;
            points = 0;
        }
        if ((start == true || (multiplicador / 3 == 2 || multiplicador / 3 == 1)) || GameObject.FindGameObjectWithTag("Target") == false)
        {

            Instantiate(Targets, Target1, QuatTar);
            Instantiate(Targets, Target2, QuatTar);
            Instantiate(Targets, Target3, QuatTar);
        }
        if (start == true)
        {

            source.PlayOneShot(Soundtrack);
            vidas = 3;
            start = false;
        }
        if(vidas == 0)
        {
            source.PlayOneShot(Gameover);
            source.Stop();
        }
        
    }

    void FixedUpdate()
    {

        if (GameObject.FindGameObjectWithTag("BolaPin") != null)
        {

            if (posbolapin.y <  0)
            {
                source.Stop();
                source.PlayOneShot(Gameover);
                vidas = vidas - 1;
                GameObject.FindGameObjectWithTag("BolaPin").GetComponent<Pelota>().MenosVida();
                source.PlayOneShot(Soundtrack);
            }
            if (close == true)
            {
                if (Compuerta.transform.position.y >= -3) { 
                    Compuerta.transform.Translate(0, -3, 0);
                }
                if (RampaCompuerta.transform.position.y <= 0.891f)
                {
                    RampaCompuerta.transform.Translate(0, 0.891f, 0);
                }
                
            }   
            else
            {
                if (Compuerta.transform.position.y <= 0.711f )
                {
                    Compuerta.transform.Translate(0, 0.722f, 0);
                }
                if (RampaCompuerta.transform.position.y >= -3)
                {
                    RampaCompuerta.transform.Translate(0, -3, 0);
                }
               
            }

        }
        
        
        

    }

    public void Target()
    {
        multiplicador = multiplicador + 1;
    }

  
    public void RestartVida()
    {
       if(vidas <= 0)
       { 
        start = true;
        
        }
    }
    public void Add1Puntuacion()
    {
        
       if(multiplicador == 0)
        {
            points = points + 1;
        }
        else
        {
            points = points + (1 * multiplicador);
        }
        
    }

    public void Add5Puntuacion()
    {
        if (multiplicador == 0)
        {
            points = points + 5;
        }
        else
        {
            points = points + (5 * multiplicador);
        }
    }

    public int GetPuntuacion()
    {
        return points;
    }

    public int GetVidas()
    {
        return vidas;
    }
}
