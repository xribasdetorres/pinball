﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lanzador : MonoBehaviour {

    SpringJoint spring;
    Rigidbody rb;

    float maxPosition = 3F;
    float decsSpeed = 1;

    private bool downPressedDown, downPressed, downPressedUp;


	// Use this for initialization
	void Start () {
        spring = GetComponent<SpringJoint>();
        rb = GetComponent<Rigidbody>();
        downPressedDown = downPressed = downPressedUp = false;

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("space"))
        {
            downPressedDown = true;
            downPressed = false;
            downPressedUp = false;
        }
        else if(Input.GetKey("space"))
        {
            downPressedDown = false;
            downPressed = true;
            downPressedUp = false;
        }
        else if(Input.GetKeyUp("space"))
        {
            downPressedDown = false;
            downPressed = false;
            downPressedUp = true;
        }
        else
        {
            downPressedDown = false;
            downPressed = false;
            downPressedUp = false;
        }
	}

    void FixedUpdate()
    {
        if(downPressedDown)
        {
            spring.maxDistance = maxPosition;
        }

        if (downPressed)
        {
            rb.AddForce(0, 0, -100, ForceMode.Acceleration);
        }

        if(downPressedUp)
        {
            spring.maxDistance = 0;
        }
    }
}
