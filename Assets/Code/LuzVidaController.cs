﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzVidaController : MonoBehaviour {

    private Points controller;
    private Light LuzVida3;
    private Light LuzVida2;
    private Light LuzVida1;
    

    // Use this for initialization
    void Start () {
        controller = GameObject.FindGameObjectWithTag("control").GetComponent<Points>();
        LuzVida3 = GameObject.Find("Vida3").GetComponent<Light>();
        LuzVida2 = GameObject.Find("Vida2").GetComponent<Light>();
        LuzVida1 = GameObject.Find("Vida1").GetComponent<Light>();
    }
	
	// Update is called once per frame
	void Update () {
		if(controller.GetVidas() == 2)
        {
            LuzVida3.color = Color.red;
        }
        if (controller.GetVidas() == 1)
        {
            LuzVida1.color = Color.red;
        }
        if (controller.GetVidas() == 0)
        {
            LuzVida3.color = Color.red;
            LuzVida2.color = Color.red;
            LuzVida1.color = Color.red;
        }
        if (controller.GetVidas() == 3)
        {
            LuzVida3.color = Color.green;
            LuzVida2.color = Color.green;
            LuzVida1.color = Color.green;
        }
    }
}
