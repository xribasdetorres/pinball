﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    public AudioClip target;
   
   
    private Points control;
    private AudioSource source;

	// Use this for initialization
	void Start () {


        control = GameObject.FindGameObjectWithTag("control").GetComponent<Points>();
        source = GetComponent<AudioSource>();
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision coll)
    {
        if(coll.gameObject.tag == "BolaPin")
        {
            
            source.PlayOneShot(target);
            control.Target();
            Destroy(gameObject);

        }
    }
    
    public void Restart()
    {
        Destroy(gameObject);
    }

}
