﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pala1 : MonoBehaviour {

    public float Fuerza = 4f;

    private JointSpring spring;
    private HingeJoint hingeJoint = null;
    public float restPosition = 0F;
    public float pressedPosition = 45F;
    public float flipperStrength = 2000F;
    public float flipperDamper = 100F;
    public float direction;

    private float turn;

    //private float

    // Use this for initialization
    void Start () {
        hingeJoint = GetComponent<HingeJoint>();


        spring = new JointSpring();
        spring.spring = flipperStrength;
        spring.damper = flipperDamper;
        spring.targetPosition = restPosition;
        hingeJoint.spring = spring;
        hingeJoint.useSpring = true;

	}
	
	// Update is called once per frame
	void FixedUpdate () {

        turn = Input.GetAxis("Horizontal");

        if (turn > 0.1)
        {
            spring.targetPosition = pressedPosition;
            hingeJoint.spring = spring;
        
        }else
        {
            spring.targetPosition = restPosition;
            hingeJoint.spring = spring;
        }    
    
	}
}
