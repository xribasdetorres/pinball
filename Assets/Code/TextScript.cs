﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScript : MonoBehaviour {

    

    private Points controlcode;
    private Text puntuacion;

    // Use this for initialization
	void Start () {
        controlcode = GameObject.FindGameObjectWithTag("control").GetComponent<Points>();
        puntuacion = gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        puntuacion.text = "" + controlcode.GetPuntuacion();
	}
}
